#!/bin/perl
system ("qhost -q > host_config.txt");
open (OUT, ">queue-hardware.tsv");

open (IN, "host_config.txt");
@line = <IN>;
for ($i=0; $i<scalar(@line); $i++) {
	if ($line[$i] =~ /^m/) {
		@fields = split(/\s+/, $line[$i]);
		$host = $fields[0];
		$cores = $fields[4];
		$mem = $fields[7];
		$mem =~ s/G//;
		#print "mem is $mem\n";
		if (($mem > 0) && ($mem <= 128) ) {$mem = "128GB";}
		if (($mem > 128) && ($mem <= 256) ) {$mem = "256GB";}
		if (($mem > 256) && ($mem <= 512) ) {$mem = "512GB";}
		if (($mem > 512) && ($mem <= 1024 ) ) {$mem = "1TB";}
		if (($mem > 1024) && ($mem <= 2048 ) ) {$mem = "2TB";}
		$i++;
		until (($line[$i] =~ /^m/) || ($i>scalar(@line))) {
			@junk = split(/\s+/, $line[$i]);
			$queues = "$queues"." $junk[1]";
			$i++;
		}
		$i--;
		print OUT "$cores\t$mem\t$queues\n";
		$queues="";
	}
}