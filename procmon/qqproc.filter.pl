#!/usr/bin/env perl
use strict;
use warnings;
use Time::Local;

#
# Clean up raw output from get_procmon_multiday.sh script
#
# Trims a lot of crud, makes the output more amenable to downstream analysis
#

my ($project,$start,$cpu,$user,$command,$mSize,$mResident,$ioRead,$ioWrite,$job,$nThreads);
my ($text,$qqproc,$date,$out,$i,$j,$k,$first,$ymd,$year,$mon,$mday,$tstart,$tmin,$tmax);

$qqproc = '/global/syscom/nsg/opt/genepool-procmon/automton/genepool/prod-v2.7.1-84-gc6e67d6/bin/qqproc';
select STDOUT; $|=1;

open SANITY, ">>sanity.csv" or die "open sanity.csv: $!\n";
$k=0;
$year = 2016;
while ( $year == 2016 ) {
  -f 'stop' && die "Abandoning because of 'stop' file\n";
  $k++;
  $date = `date +%Y-%m-%d --date="$k days ago"`;
  chomp $date;
  $out = "qqproc.$date.csv";
  print "Processing for $date\n";
  -f $out && next;
  $ymd = $date;
  $ymd =~ s%-%,%g;
  ($year,$mon,$mday) = split( ',', $ymd );
  open OUT, ">$out" or die "open $out: $!\n";

# Prepare for sanity-checks
  $tmax = $tstart = timelocal( 0, 0, 0, $mday, $mon-1, $year );
  $tmin = $tmax + 86401;

  open QQ, "$qqproc -q 'script==\"\"' -c project,startTime,cpuTime_net,user,command,m_size,m_resident,io_readBytes,io_writeBytes,job,numThreads -S $date -E $date |" or die "qqproc: $!\n";

  $_ = <QQ>; # skip filePath line

  $i = $j = 0;
  while ( <QQ> ) {
    $j++;
    next if m%^Unknown,%;
    m%^[^,]+,([^,]+),([^,]+),% or next;
    $start = $1;
    $cpu = $2;
    next if $cpu < 60;
    if ( $start > $tmax ) { $tmax = $start; }
    if ( $start < $tmin ) { $tmin = $start; }

    print SANITY $k, ',', $start - $tstart, "\n";
    print OUT $ymd, ',', $_;
    if ( ! (++$i%100) ) { print "  $i/$j \r"; }
  }
  if ( $j ) {
    print "All done for $date ($i/$j entries, ",int(10000*$i/$j)/100,"%)\n";
    $tmin -= $tstart;
    $tmax = $tstart + 86400 - $tmax;
#   if ( $tmin < 0 ) { die "TMin out of bounds ($tmin < 0)\n"; }
    if ( $tmax > 86400 ) { die "TMax out of bounds ($tmax > 86400)\n"; }
    print 'start/end interval: ', $tmin, ' ', $tmax, "\n";
  } else {
    print "All done for $date (0 entries!)\n";
  }

  close OUT;
}
close SANITY;

print "All done!\n";
