#!/usr/bin/env perl
use warnings;
use strict;

my ($in,$out,$user,$time,$bin,$dir,$args);
my ($start,$bytesIn,$bytesOut,$mRes,$mSize);
my ($minTime,$n);

$in = 'gp-raw.csv';
$out = 'gp-filtered.csv';
$minTime = 10;

open  IN,  "<$in" or die   "read $in: $!\n";
open OUT, ">$out" or die "write $out: $!\n";
print OUT "User,Time,Start,bytesIn,bytesOut,mRes,mSize,Bin,Dir,Args\n";;

$n=0;
while ( <IN> ) {
  $n++;
  chomp;
  next if m%^\s+%;
  ($user,$time,$start,$bytesIn,$bytesOut,$mRes,$mSize,$bin) = split(',', $_);
  next unless ( defined($time) && defined($user) && defined($bin) );
  # defined($time) || die "Time not defined for \"$_\" at $n\n";
  next unless $time > $minTime;
  # $user || die "User not defined for \"$_\" at $n\n";
  # $bin  || die "Bin not defined for \"$_\" at $n\n";
  $bin =~ m%^([^|]*/)?([^|]*)([|])?(.*)$%;
  $dir = $1 || '';
  $bin = $2;
  $args = $4 || '';
  $args =~ s%\|% %g;
  print OUT "$user,$time,$start,$bytesIn,$bytesOut,$mRes,$mSize,$bin,$dir,$args\n";
  print "$user $bin\n";
}

print "All done\n";
