#!/usr/bin/env perl
use warnings;
use strict;
use Getopt::Long;
my ($help,$verbose,$debug);
my ($cmd,$state,$host,$queue,$jobid,$slots,%h);

$debug = 1;
GetOptions(
  'help'    => \$help,
  'debug'   => \$debug,
  'verbose' => \$verbose,
);

$help && die "No help yet, don't hold your breath...\n";

$cmd = "qhost -q -j";
open Q, "$cmd |" or die "'$cmd': $!\n";
# open Q, "<host_config_jobs.txt" or die "host_config_jobs: $!\n";

# discard first four lines, they have no useful information
$_ = <Q>;
$_ = <Q>;
$_ = <Q>;
$_ = <Q>;

while ( $_ = <Q> ) {

  # Is this a hostname segment
  if ( m%^(\S+)\s+\S+\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\S+)\s+(\S+)\s+(\S+)\s+% ) {
    $host = $1;
    $debug && print "Found host $host\n";
    $h{ $host }{ config } = { 'Ncpu' => $2, 'Nsoc' => $3, 'Ncor' => $4, 'Nthr' => $5, 'Load' => $6, 'MemTot' => $7, 'MemUse' => $8 };
  }

  # ...or a queue-name segment?
  if ( m%^\s*([a-z._-]+)\s+% ) {
    $queue = $1;
    $debug && print "Found queue $queue\n";
    $h{ $host }{ queues }{ $queue } = {};
  }

  # ...or a batch job segment?
  if ( m%^\s*(\d+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)% ) {
    $jobid = $1;
    $debug && print "Found job $jobid\n";
    $h{ $host }{ queues }{ $queue }{ $jobid } = { 'Priority' => $2, 'Name' => $3, 'User' => $4, 'State' => $5 };
  }

}

$DB::single=1;
1;
