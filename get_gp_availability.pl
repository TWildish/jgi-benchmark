#!/bin/perl
use Data::Dumper;

#system ("qhost -q > host_config.txt");

open (OUT, ">queue-hardware.tsv");

#open (IN, "host_config.txt");
open IN, "qhost -q |";
@line = <IN>;
for ($i=0; $i<scalar(@line); $i++) { 
	if ($line[$i] =~ /^m/) {
		@fields = split(/\s+/, $line[$i]);
		$host = $fields[0];
		$cores = $fields[4];
		$mem = $fields[7];
		$mem =~ s/G//;
		#print "mem is $mem\n";
		if (($mem > 0) && ($mem <= 128) ) {$mem = "128GB";}
		if (($mem > 128) && ($mem <= 256) ) {$mem = "256GB";}
		if (($mem > 256) && ($mem <= 512) ) {$mem = "512GB";}
		if (($mem > 512) && ($mem <= 1024 ) ) {$mem = "1TB";}
		if (($mem > 1024) && ($mem <= 2048 ) ) {$mem = "2TB";}
		$i++;
		until (($line[$i] =~ /^m/) || ($i>scalar(@line))) {
			@junk = split(/\s+/, $line[$i]);
			$runstring = $junk[3];
			$qerrorstate = $junk[4];
			($unknown, $running, $totalslots) = split(/\//, $runstring);
			if ($qerrorstate) {$errorstate=$qerrorstate;}
			if ($total_avail =~ "zero") {$total_avail = $cores - $running;}
			else {$total_avail = $total_avail - $running;}
			$avail_slots = $totalslots - $running;
			if ( ($junk[1] =~ /xfer/) || ($junk[1] =~ /normal/) || ($junk[1] =~ /long/) || ($junk[1] =~ /high/) ) {
				$queues = "$queues"." $junk[1] ($avail_slots) | ";
			}
			$i++;
		}
		$i--;
		if ($errorstate) {$total_avail = 0; push @sloterror, $outstr;}
		push my @sloterror, $errorstate;
		$outstr = "$cores\t$mem\t$queues\t$errorstate\tSlots available=$total_avail\n";
#		print $outstr;
		if ($outstr =~ /_excl\.q/) {push @exclusive, $outstr;}
		push @all, $outstr;
		$total_avail="zero";
		$queues="";
		$errorstate=""; 
		$qerrorstate="";
	}
}
foreach $line (@exclusive) {
	#print $line;
	@fields = split(/\t/, $line);
	$nodehardware = "$fields[0]"."core "."$fields[1]";
	($junk,$slotsavail) = split(/=/, $fields[-1]);
	$errorstate = $fields[3];
	$queuestring = $fields[2];
	$queuestring =~ s/\d//g;
	$queuestring =~ s/high\.q \(\) \|  //;
	$queuestring =~ s/\(\)//g;
	$queuestring =~ s/ //g;
	$queuestring =~ s/\|/ /g;
	$queuestring =~ s/\.q//g;
	chop $queuestring;
	chomp $slotsavail;
	if ($slotsavail == $fields[0]) {$nodeavailable = 1;}
	else {$nodeavailable = 0;}
	if ($nodeavailable || $errorstate) {
		$str = "$nodehardware\t($queuestring)";
		if ($errorstate) {$str = "$str"." in error state $errorstate";}
#		print "$str\n";
		push @excl_available, $str;
		
	}
}
#print scalar localtime."\n";
print "\nExclusive node availability:\n";
my %counts;
my @unique = grep !$counts{$_}++, @excl_available;
foreach (sort keys %counts) {
	if (!($_ =~ /error/)) {
		$something_there=1;
		($hardware, $queues) = split(/\t/, $_);
		print "\t$hardware\t$counts{$_} nodes available with $queues\n";next;
	}
}
if (!$something_there) {print "\tNo nodes are available exclusively right now!\n";}

print "  Unavailable nodes:\n";
foreach (sort keys %counts) {
	if ($_ =~ /error/) {
		($hardware, $queues) = split(/\t/, $_);
		print "\t$hardware\t$counts{$_} nodes unavailable: $queues\n";next;
	}
}

print "\nSlotted queues availability\n";
foreach $line (@all) {
	@fields = split(/\t/, $line);
	$queuestring = $fields[2];
	@queues = split(/\|/, $queuestring);
	foreach $queuesubstr (@queues) {
		($queue, $num) = split(/ \(/, $queuesubstr);
		$num =~ s/\D//g;
		if ($queue =~ /normal.q/) {$normalcount += $num;}
		if ($queue =~ /high.q/) {$highcount += $num;}
		if ($queue =~ /long.q/) {$longcount += $num;}
	}
}

print "\tnormal.q\t$normalcount slots available\n";
print "\thigh.q\t\t$highcount slots available\n";
print "\tlong.q\t\t$longcount slots available\n";
foreach $line (@sloterror) {
	@fields = split(/\t/, $line);
	$errorstate=$fields[3];
	if ($errorstate) {push @sloterrorstate, $errorstate;}
}
my $numsloterror = scalar(@sloterrorstate);
print "  $numsloterror slotted nodes in an error state\n";

my %slotcount;
my @unique = grep !$slotcount{$_}++, @sloterrorstate;
foreach (sort keys %slotcount) {
	print "\t$slotcount{$_} nodes in error state $_\n";next;
}
