#!/usr/bin/env perl
use warnings;
use strict;
use JSON::XS;
use Data::Dumper;

my ($cmd,$json,$h);
$cmd = "qs --style json";
open Q, "$cmd |" or die "$cmd: $!\n";

$json = <Q>;
$h = decode_json( $json );

print Dumper( $h->[0] );
