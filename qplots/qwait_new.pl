#!/usr/bin/env perl

use strict;
use warnings;

my ($in,$out,$epoch,$submitted,$jobid,$queue,$owner,$project,$wait,%h);

$in  = "qwait.csv";
$out = "qwait_new.csv";

open  IN, "<$in"  or die "open $in: $!\n";
open OUT, ">$out" or die "open $out: $!\n";
$_ = <IN>; # discard header
print OUT "submitted,jobid,wait,queue,owner,project\n";

while ( <IN> ) {
  chomp;
  ($epoch,$submitted,$jobid,$queue,$owner,$project) = split( ',', $_ );
  $h{ $jobid } = [ $submitted, $jobid, $epoch - $submitted, $queue, $owner, $project ];
}

foreach $jobid ( sort keys %h ) {
  print OUT join( ',', @{ $h{ $jobid } } ), "\n";
}