# library(markdown)

navbarPage( theme = shinytheme("cerulean"), "Genepool Monitoring",

  tabPanel("Queue History",

    sidebarLayout(

      sidebarPanel(

        headerPanel( "Queue history" )

        ,p("Queues are sampled every 5 minutes.")
        ,br()

        ,dateRangeInput(
          inputId = "dateRange",
          label   = "Date Range (inclusive)",
          start   = today - 2,
          end     = today,
          min     = qhMinDate,
          max     = today,
        )

        ,checkboxGroupInput(
          input    = "states",
          label    = "Select states to show",
          choices  = list( "running", "pending", "hold", "error" ),
          selected = list( "running", "pending" )
        )

        ,checkboxInput(
          input = "logY",
          label = "Show log scale in Y",
          value = FALSE
        )

      )

      ,mainPanel(
        plotOutput( "qhistory" )
      )
    )

  )

  ,tabPanel("Current Wait Times",
    sidebarLayout(

      sidebarPanel(

        headerPanel( "Queue wait times" )

        ,p("Note the logarithmic x-axis, y-axes are set per-plot.")
        # ,p("The dashed red line indicates 12 hours.")
        ,br()

        ,checkboxGroupInput(
          input    = "projects",
          label    = "Select projects to show",
          choices  = projects,
          selected = c()
        )

        ,actionButton("selectall", label="Select/Deselect all")

      )

      ,mainPanel(
        plotOutput( "qwait" )
      )
    )

  )

  ,tabPanel("Wait Time History",
    sidebarLayout(

      sidebarPanel(

        headerPanel( "Queue wait times" )

        ,br()
        ,p("Graphs show median and quartile-range of wait times per timebin.")
        ,p("N.B. y-axis is logarithmic, and set per-plot. Bin-width is set as a function of the date interval.")
        ,br()
        ,p(" ", id="wth_binwidth")
        ,br()

        ,dateRangeInput(
          inputId = "dateRangeWTH",
          label   = "Date Range (inclusive)",
          start   = today,
          end     = today,
          min     = qwMinDate,
          max     = today,
        )

      )

      ,mainPanel(
        plotOutput( "qwaithistory" )
      )
    )

  ),

  useShinyjs()

)
