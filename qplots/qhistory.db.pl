#!/usr/bin/env perl
use strict;
use warnings;
use DBI;
use Getopt::Long;

my ($driver,$host,$database,$userid,$password,$dbconfig);
my ($epoch,$queue,$pending,$running,$hold,$error,$csv);
my ($dbh,$sth,$dsn);

$csv = $ENV{HOME} . "/qhistory.csv";
$dbconfig = "dbconfig";

GetOptions(
  "csv=s"      => \$csv,
  "dbconfig=s" => \$dbconfig,
);

open DBCONFIG, "<$dbconfig" or die "$dbconfig: $!\n";
$_ = <DBCONFIG>; # discard header
$_ = <DBCONFIG>;
chomp;
($driver,$host,$database,$userid,$password) = split( '\|', $_ );

$dsn = "DBI:$driver:database=$database;host=$host";
$dbh = DBI->connect($dsn, $userid, $password, { AutoCommit => 0, RaiseError => 1 } ) or die $DBI::errstr;
$sth = $dbh->prepare("INSERT IGNORE INTO queue_history
                       ( epoch, queue_id, pending, running, hold, error )
                        values
                       ( ?, (select id from queues where queue = ?), ?, ?, ?, ? )" );

open CSV, "<$csv" or die "open $csv: $!\n";
$_ = <CSV>; # discard header
while ( <CSV> ) {
  ( $epoch, $queue, $pending, $running, $hold, $error ) = split( ',', $_ );
  $sth->execute( $epoch, $queue, $pending, $running, $hold, $error )
          or die $DBI::errstr;
}
$sth->finish();
$dbh->commit or die $DBI::errstr;
